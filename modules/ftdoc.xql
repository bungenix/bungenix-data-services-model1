xquery version "3.1";

module namespace ftdoc="http://bungeni.com/bungenix/xq/db/ftdoc";

declare namespace aknft="http://bungeni.com/ns/1.0/content/pdf";

import module namespace common="http://bungeni.com/bungenix/xq/db/common" at "common.xql";

declare function ftdoc:doc(
        $this-iri as xs:string
    ) {
    let $coll := common:doc-fulltext-collection()
    let $doc := 
            $coll//aknft:pages[@connectorID eq $this-iri ]/parent::node()
     return $doc
};