xquery version "3.1";

(:~
 : This module has data functions for retrieving AKN documents from the XML database.
 : THere is no higher processing or transformation of the documents done in this module
 : @author Ashok Hariharan
 : @version 1.0
 :)
module namespace common="http://bungeni.com/bungenix/xq/db/common";
declare namespace bx="http://bungeni.com/ns/1.0";
declare namespace bxd="http://bungeni.com/ns/1.0/data";
declare namespace an="http://docs.oasis-open.org/legaldocml/ns/akn/3.0";
import module namespace config="http://bungeni.com/bungenix/xq/db/config" at "config.xqm";

declare function common:doc-collection() {
    let $sc := config:storage-config("legaldocs")
    return collection($sc("collection"))
};

declare function common:doc-fulltext-collection() {
    let $sc := config:storage-config("legaldocsText")
    return collection($sc("collection"))
};
