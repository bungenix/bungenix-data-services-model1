xquery version "3.1";

(:~
 : This module has data functions for retrieving AKN documents from the XML database.
 : THere is no higher processing or transformation of the documents done in this module
 : @author Ashok Hariharan
 : @version 1.0
 :)
module namespace caching="http://bungeni.com/bungenix/xq/db/caching";
declare namespace bx="http://bungeni.com/ns/1.0";
declare namespace bxd="http://bungeni.com/ns/1.0/data";
declare namespace an="http://docs.oasis-open.org/legaldocml/ns/akn/3.0";

import module namespace config="http://bungeni.com/bungenix/xq/db/config" at "config.xqm";
import module namespace andoc="http://exist-db.org/xquery/apps/akomantoso30" at "akomantoso.xql";
import module namespace countries="http://bungeni.com/bungenix/xq/portal/countries" at "countries.xql";
import module namespace langs="http://bungeni.com/bungenix/xq/portal/langs" at "langs.xql";

declare function local:doc-collection() {
    let $sc := config:storage-config("legaldocs")
    return collection($sc("collection"))
};

declare function caching:get-metadata() {
    let $sc := config:storage-config("legaldocs")
    let $docs := local:doc-collection()//an:akomaNtoso
    return
    <metadata source="{$sc("db-path")}">
    {
    for $kw in $docs//an:classification/an:keyword
        let $kw-shows := $kw/@showAs
        group by $kwv := data($kw/@value)
        order by $kwv ascending
        return <keyword value="{$kwv}" >{$kw-shows[1]}</keyword>
     }
     </metadata>  
};



(:
 : There may be multiple instances of FRBRcountry elements if the doc has attachments.
 : Consider only the first one for grouping since the value will be the same per doc.
 : Same holds for langs and years. 
:)
declare function caching:countries-filter-cache() {
    let $docs := local:doc-collection()//an:akomaNtoso
    return
        <filters timestamp="{string(current-dateTime())}">
            <filter name="countries" label="Countries"> {
              
            for $doc in $docs
              group by $country := data($doc//an:FRBRcountry/@value)[1]
              order by $country ascending
              return <country code="{$country}" count="{count($doc)}" >
                       {countries:country-name-alpha2($country)}
                      </country>              
            } 
            </filter>
        </filters>
};


declare function caching:langs-filter-cache() {
    let $docs := local:doc-collection()//an:akomaNtoso
    return
        <filters timestamp="{string(current-dateTime())}">
            <filter name="langs" label="Languages">
            {
            for $doc in $docs
              group by $lang := data($doc//an:FRBRlanguage/@language)[1]
              order by $lang ascending
              return <lang code="{$lang}" count="{count($doc)}" >{langs:lang3-name($lang)}</lang>
            }                
            </filter>
        </filters>
};

declare function caching:years-filter-cache() {
    let $docs := local:doc-collection()//an:akomaNtoso
    return
        <filters timestamp="{string(current-dateTime())}">
             <filter name="years" label="Years">
                {
                for $doc in $docs
                  group by $year := 
                  try {
                      year-from-date($doc//an:FRBRExpression/an:FRBRdate/@date)
                  } catch * {
                      0000
                  }
                  order by $year descending
                  where $year > 0000
                  return <year year="{$year}" count="{count($doc)}" ></year>
                }
            </filter>
        </filters>
};

declare function caching:keywords-filter-cache() {
    let $docs := local:doc-collection()//an:akomaNtoso
    return
        <filters timestamp="{string(current-dateTime())}">
              <filter name="keywords" label="Subjects">
                {
                for $kw in $docs//an:keyword
                let $kw-shows := $kw/@showAs
                  group by $kwv := data($kw/@value)
                  order by $kwv ascending
                  return <keyword value="{$kwv}" count="{count($kw)}" >{$kw-shows[1]}</keyword>
                }                
                            
            </filter>  
        </filters>
};

declare function caching:types-filter-cache() {
    let $docs := local:doc-collection()//an:akomaNtoso
    return
        <filters timestamp="{string(current-dateTime())}">
              <filter name="types" label="Document Types"> {
              
            for $doc in $docs
              group by $type := data($doc/
              (
                an:act|
                an:amendment|
                an:amendmentList|
                an:bill|
                an:debate|
                an:debateReport|
                an:doc|
                an:documentCollection|
                an:judgment|
                an:officialGazette|
                an:portion|
                an:statement
                )
                /@name)
              order by $type ascending
              return <type type="{$type}" count="{count($doc)}" />             
            }</filter>
        </filters>
};


declare function caching:filter-cache() {
    let $docs := local:doc-collection()//an:akomaNtoso
    return
        <filters timestamp="{string(current-dateTime())}">
            <filter name="countries" label="Countries"> {
              
            for $doc in $docs
              group by $country := data($doc/an:*/an:meta//an:FRBRcountry/@value)
              order by $country ascending
              return <country code="{$country}" count="{count($doc)}" >
                       {countries:country-name-alpha2($country)}
                      </country>              
            } 
            </filter>
            <filter name="langs" label="Languages">
            {
            for $doc in $docs
              group by $lang := data($doc/an:*/an:meta//an:FRBRlanguage/@language)
              order by $lang ascending
              return <lang code="{$lang}" count="{count($doc)}" >{langs:lang3-name($lang)}</lang>
            }                
            </filter>
            <filter name="years" label="Years">
                {
                for $doc in $docs
                  group by $year := year-from-date($doc/an:*/an:meta//an:FRBRExpression/an:FRBRdate/@date)
                  order by $year descending
                  return <year year="{$year}" count="{count($doc)}" ></year>
                }
            </filter>
            <filter name="keywords" label="Subjects">
                {
                for $kw in $docs/an:*/an:meta//an:classification/an:keyword
                let $kw-shows := $kw/@showAs
                  group by $kwv := data($kw/@value)
                  order by $kwv ascending
                  return <keyword value="{$kwv}" count="{count($kw)}" >{$kw-shows[1]}</keyword>
                }                
                            
            </filter>  
             <filter name="types" label="Document Types"> {
              
            for $doc in $docs
              group by $type := data($doc/
              (
                an:act|
                an:amendment|
                an:amendmentList|
                an:bill|
                an:debate|
                an:debateReport|
                an:doc|
                an:documentCollection|
                an:judgment|
                an:officialGazette|
                an:portion|
                an:statement
                )
                /@name)
              order by $type ascending
              return <type type="{$type}" count="{count($doc)}" />             
            }</filter>
        </filters>

};
