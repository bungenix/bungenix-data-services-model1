xquery version "3.1";

(:~
 : This module has data functions for retrieving AKN documents from the XML database.
 : THere is no higher processing or transformation of the documents done in this module
 : @author Ashok Hariharan
 : @version 1.0
 :)
module namespace data="http://bungeni.com/bungenix/xq/db/data";
declare namespace bx="http://bungeni.com/ns/1.0";
declare namespace bxd="http://bungeni.com/ns/1.0/data";
declare namespace gft="http://bungeni.com/ns/1.0/content/pdf";
declare namespace an="http://docs.oasis-open.org/legaldocml/ns/akn/3.0";

import module namespace config="http://bungeni.com/bungenix/xq/db/config" at "config.xqm";
import module namespace andoc="http://exist-db.org/xquery/apps/akomantoso30" at "akomantoso.xql";
import module namespace common="http://bungeni.com/bungenix/xq/db/common" at "common.xql";
import module namespace langs="http://bungeni.com/bungenix/xq/portal/langs" at "langs.xql";
import module namespace utils="http://bungeni.com/bungenix/xq/portal/utils" at "./utils.xql";
import module namespace dbauth="http://bungeni.com/bungenix/xq/portal/dbauth" at "./dbauth.xql";
import module namespace ftdoc="http://bungeni.com/bungenix/xq/db/ftdoc" at "ftdoc.xql";
import module namespace functx="http://www.functx.com" at "./functx.xql";

declare function data:doc($this-iri as xs:string) {
    let $coll := common:doc-collection()
    return andoc:find-document($coll, $this-iri)
};


(:~
 : Returns the Page IDs of a given document containing the search term
 : @param $this-iri the expression-this iri of the document
 : @params $term search term to look for in the document
 : @returns Page IDs containing the search term
 :)
declare function data:doc-fulltext-search($this-iri as xs:string, $term as xs:string) {
    let $coll := common:doc-fulltext-collection()
    let $lucene_query :=
    <query>
        <phrase slop="3">{$term}</phrase>
    </query>
    let $lucene_pageIDs := $coll//gft:pages[@connectorID eq $this-iri]/gft:page[ft:query(., $lucene_query)]
    let $ngram_pageIDs := $coll//gft:pages[@connectorID eq $this-iri]/gft:page[ngram:contains(., $term)]
    
    let $pageIDs := (data($lucene_pageIDs/@id), data($ngram_pageIDs/@id))

    return fn:distinct-values($pageIDs)
};

(:~
 : Returns the akn documents containing the search term
 : @params $term search term to look for in the document
 : @returns akn docs containing the search term
 :)
declare function data:coll-fulltext-search($term as xs:string) {
     subsequence(data:coll-fulltext-search-all-results($term), 1, 5)
};


(:~
 : Returns the akn documents containing the search term
 : @params $term search term to look for in the document
 : @returns akn docs containing the search term
 :)
declare function data:coll-fulltext-search-all-results($term as xs:string) {
    let $coll := common:doc-fulltext-collection()
    let $lucene_query :=
    <query>
        <phrase slop="3">{$term}</phrase>
    </query>

    let $search-result-docs :=
        for $doc in $coll//gft:pages
            let $connectorID := data($doc/@connectorID)
            let $lucene_pageIDs := $doc/gft:page[ft:query(., $lucene_query)]
            let $ngram_pageIDs := $doc/gft:page[ngram:contains(., $term)]
            let $pageIDs := (data($lucene_pageIDs/@id), data($ngram_pageIDs/@id))
            let $distinct := fn:distinct-values($pageIDs)
            return
                if (empty($distinct)) then
                    ()
                else
                    data:doc($connectorID)
     return $search-result-docs
};

(:~
 : Returns the 'n' most recent documents in the System as per updated date
 : @param $count integer value indicating max number of documents to return
 : @returns bxd envelop with upto 'n' AKN documents
 :)
declare function data:recent-docs-full($count as xs:integer, $from as xs:integer) {
    (: call the HOF :)
    let $func := local:full-doc#1
    return
        local:recent-docs($func, $count, $from)
};

(:~
 : Returns the summary of 'n' most recent documents in the Systme as per updated date
 : @param $count integer value indicating max number of documents to return
 : @returns bxd envelope with upto 'n' AKN document summaries
 :)
declare function data:recent-docs-summary($count as xs:integer, $from as xs:integer) {
    (: call the HOF :)
    let $func := data:summary-doc#1
    return
        local:recent-docs($func, $count, $from)
};

(:~
 : This API is used by a SERVER side api, not meant for client consumption. Allows 
 : sending constructed XQueries to the data server. The constructed query is executed within
 : a collection context XPath. 
 : @param $count integer value indicating max number of documents to return
 : @param $from from where to start returning the documents
 : @param $qry the constructed XQuery
 : @returns bxd envelop with upto 'n' AKN documents
 :)
declare function data:search-filter($count as xs:integer, $from as xs:integer, $qry as xs:string) {
    (: This is the higher order function to be passed to the search api, it generates the abstract :)
    let $func := data:summary-doc#1
    return
        local:search-filter-docs($func, $count, $from, $qry)
};

declare function data:advance-search-filter($count as xs:integer, $from as xs:integer, $qry as xs:string) {
    (: This is the higher order function to be passed to the search api, it generates the abstract :)
    let $func := data:summary-doc#1
    return
        local:advance-search-filter-docs($func, $count, $from, $qry)
};


declare function data:search-country-summary($country as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $func := data:summary-doc#1
    return
        local:search-country-docs($func, $country, $count, $from)
};

declare function data:search-language-summary($doclang as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $func := data:summary-doc#1
    return
        local:search-language-docs($func, $doclang, $count, $from)
};

declare function data:search-years-summary($year as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $func := data:summary-doc#1
    return
        local:search-year-docs($func, $year, $count, $from)
};

declare function data:search-keywords-summary($kw as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $func := data:summary-doc#1
    return
        local:search-keyword-docs($func, $kw, $count, $from)
};


(:~
 : Returns the summary of 'n' most recent documents in the Systme as per updated date
 : @param $count integer value indicating max number of documents to return
 : @returns bxd envelope with upto 'n' AKN document summaries
 :)
declare function data:theme-docs-summary($themes as xs:string*, $count as xs:integer, $from as xs:integer) {
    (: call the HOF :)
    let $func := data:summary-doc#1
    return
        local:theme-docs(
            $func, 
            $themes, 
            $count, 
            $from
        )
};


(:~
 : Returns documents grouped by Work, returns the 10 most recent works, 
 : and within that all the expressions ordered by date, newest first.
 : @param $count integer value indicating max number of works to return
 : @returns bxd envelope with upto 'n' AKN Works, and within each work its expressions
 :)
declare function data:recent-works($count as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll := collection($sc("collection"))
    let $docs := $coll//an:akomaNtoso/parent::node()
    (:
     : Note: !+(AH, 2017-07-26) This may cause a performance problem in the future.
     : Eventually this can be adapted into a cached index of grouped works
     : built by a back-end trigger, which means we don't do the group by dynamically
     :)
    let $by-works :=
        for $works in $docs
            let $work-this := $works//an:FRBRWork/an:FRBRthis/@value
            group by $work-this
            order by $works//an:FRBRWork/an:FRBRdata/@date descending
            return
               <bxd:work iri="{$work-this}" ordered-by="dt-expr-date-desc" xmlns:bxd="http://bungeni.com/ns/1.0/data"> {
                    for $expr in $works
                     order by $expr//an:FRBRExpression/an:FRBRdate/@date descending
                     return
                       data:summary-doc($expr)
               }</bxd:work>
     return
        for $work in subsequence($by-works,1, $count) 
            return
                $work
};

declare
function data:get-expression-document-chain($iri as xs:string) {
    let $sc := config:storage-config("legaldocs")
    let $coll := collection($sc("collection"))
    let $doc := data:doc($iri)
    let $work-iri := andoc:work-FRBRthis-value($doc)
    let $doc-chain := $coll//an:akomaNtoso[
            ./an:*/an:meta/
                    an:identification[
                        an:FRBRWork/
                            an:FRBRthis/@value eq $work-iri
                       ]
            ]/parent::node()
    return
         <bxd:work iri="{$work-iri}" ordered-by="dt-expr-date-ascending" xmlns:bxd="http://bungeni.com/ns/1.0/data"> {
                for $item in $doc-chain
                    order by $item//an:FRBRExpression/an:FRBRdate/@date ascending
                    return data:summary-doc($item)
            }
         </bxd:work>
            
};


declare function local:get-thumbnail-name($doc) {
   "th_" || 
   replace(
    substring-before(util:document-name($doc), ".xml"),
    "@", 
    ""
    ) || 
   ".png"
};

(:~
 : 
 :
 :
 :)
declare 
function data:get-component-pdf($iri as xs:string) {
    let $doc := data:doc($iri)
    let $folder := util:collection-name($doc)
    let $doc-name :=  data:get-embedded-pdf-name($doc)
    return
        if (util:binary-doc-available($folder || "/" || $doc-name)) then
            util:binary-doc($folder || "/" || $doc-name)
        else
            ()
};

declare 
%private 
function 
data:get-embedded-pdf-name($doc) {
    let $component := $doc//(an:body|an:debateBody|an:mainBody|an:judgmentBody)/an:book/an:componentRef[@alt]
    let $file := data($component/@alt)    
    return $file
};


declare function data:get-thumbnail($iri as xs:string) {
    let $doc := data:doc($iri)
    let $folder := util:collection-name($doc)
    let $th-name := local:get-thumbnail-name($doc)
    return
        if (util:binary-doc-available($folder || "/" || $th-name)) then
            util:binary-doc($folder || "/" || $th-name)
        else
            ()
};


declare function data:thumbnail-available($doc) {
    let $folder := util:collection-name($doc)
    let $th-name := local:get-thumbnail-name($doc)
    return
        util:binary-doc-available($folder || "/" || $th-name)
};

(:~
 : This function is simply a higher order function to 
 : return the document as is. This is in the local namespace
 : not visible to external libraries. We need this because the 
 : because the point of use is within a higher order construct
 : @see http://exist-db.org/exist/apps/wiki/blogs/eXist/HoF
 : @param $doc AKN document
 : @returns the same input AKN document
 :)
declare function local:full-doc($doc) {
    $doc
};

(:~
 : This function extractss text from xml 
 : @param XML document and no. of character to be returned
 : @returns Text from XML document. Also special characters. 
 :)
declare function local:text-from-xml($doc, $characterLimit as xs:integer) {
    translate(
        substring(
            normalize-space(data($doc)[1]), 
            1, 
            $characterLimit
         ), 
         '-$`,:%!@#_|]$?~@#!%:;=_+*.-+=?;"', '')  
};


(:~
 : This function is called as a higher order function 
 : from data:recent-docs(), and returns a summary of the AKN document
 : @param AKN document
 : @returns Summary of the document metadta in a bxd envelope
 :)
declare function data:summary-doc($doc) {
    
    let $frbrnumber := andoc:FRBRnumber($doc)
    let $frbrcountry := andoc:FRBRcountry($doc)
    let $pdfname := $doc//an:book[@refersTo='#mainDocument']/an:componentRef/@alt
    let $pdfname-tok := tokenize($pdfname, "\.")
    let $lang-code := andoc:FRBRlanguage-language($doc)
    let $lang-name := langs:lang3-name($lang-code)
    let $characterLimit :=500
    return
    <bxd:exprAbstract expr-iri="{andoc:expression-FRBRthis-value($doc)}"
        work-iri="{andoc:work-FRBRthis-value($doc)}" xmlns:bxd="http://bungeni.com/ns/1.0/data">
        <bxd:date name="work" value="{andoc:work-FRBRdate-date($doc)}" />
        <bxd:date name="expression" value="{andoc:expression-FRBRdate-date($doc)}" />
        <bxd:type name="legislation" aknType="act" />
        <bxd:country value="{andoc:FRBRcountry($doc)/@value}" >{$frbrcountry/@showAs}</bxd:country>
        <bxd:language value="{andoc:FRBRlanguage-language($doc)}" showAs="{$lang-name}" />
        <bxd:publishedAs>{andoc:publication-showas($doc)}</bxd:publishedAs>
        <bxd:summaryText>{
        local:text-from-xml(
            ftdoc:doc(
                data(andoc:expression-FRBRthis-value($doc))
            ),
            $characterLimit
        )
        }</bxd:summaryText>        
        <bxd:number value="{$frbrnumber/@value}">{$frbrnumber/@showAs}</bxd:number>
        <bxd:componentLink src="{$doc//an:book[@refersTo='#mainDocument']/an:componentRef/@src}" value="{$doc//an:book[@refersTo='#mainDocument']/an:componentRef/@alt}" />
     </bxd:exprAbstract>
};


(:~
 : Private function that retrieves AKN documents. How these documents are returned
 : is upto the higher order function passed in as parameter 1. 
 : @param $func higher order function that accepts an AKN document as a parameter
 : @param $count max number of documents to return
 : @returns processed output as defined by the higher order function
 :)
 (:
declare function local:recent-docs($func, $count as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll := collection($sc("collection"))
    let $docs := $coll//an:akomaNtoso/parent::node()
    return
        for $doc in subsequence($docs, 1, $count)
            order by $doc//an:proprietary/bx:bungenix/bx:dateTime[
                @refersTo = '#dtUpdated'
                ]/@datetime 
            descending
        return $func($doc) 
};
:)
(:~
 : Private function that retrieves AKN documents. How these documents are returned
 : is upto the higher order function passed in as parameter 1. 
 : @param $func higher order function that accepts an AKN document as a parameter
 : @param $count max number of documents to return
 : @returns processed output as defined by the higher order function
 :)
declare function local:recent-docs($func, $count as xs:integer, $from as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll := collection($sc("collection"))
    let $docs := $coll//an:akomaNtoso/parent::node()
    let $docs-in-order := 
        for $doc in $docs
            order by sort:index("SIRecent", $doc)
        return $doc
    let $total-docs := count($docs-in-order)
    return
        if (count($docs-in-order) lt $from) then
            (: $from is greater than the number of available docs :)
            map {
                "records" := 0,
                "total-page" := 0,
                "current-page" := 0,
                "data" := 
                    <bxd:empty>
                        <bxd:message lang="eng">
                        No documents dound
                        </bxd:message>
                    </bxd:empty>
            }    
        else
            map {
                "records" := $total-docs,
                "page-size" := $count,
                "items-from" := $from,
                "total-pages" := ceiling($total-docs div $count) ,
                "current-page" := xs:integer($from div $count) + 1,
                "data" :=
                    for $s-d in subsequence($docs-in-order, $from, $count)
                        return $func($s-d)
            }            

};

(:
 : Returns a grouped summary output of a document query.
 : DOES NOT return documents.
 : Returns an XML summary of year, and number of documents in that year
 :
 :)
declare function data:search-filter-timeline(
    $qry as xs:string
    ) {
    let $sc := config:storage-config("legaldocs")
    let $all-docs := collection($sc("collection"))//an:akomaNtoso
    let $docs := util:eval( "$all-docs" || $qry || "/parent::node()" )
    let $ts := current-dateTime()
    let $total-docs := count($docs)
    return
     <timeline>
        <years timestamp="{$ts}" total="{$total-docs}">{
        for $doc in $docs
            let $year := year-from-date(xs:date(andoc:expression-FRBRdate-date($doc)))
            group by $year
            order by $year
        return <year year="{$year}" count="{count($doc)}" />}
        </years>
        <countries timestamp="{$ts}" total="{$total-docs}">{
        for $doc in $docs
            let $country := data(andoc:FRBRcountry($doc)/@value)
            group by $country
            order by $country
            return <country name="{$country}" count="{count($doc)}" />
       }</countries> 
       <languages timestamp="{$ts}" total="{$total-docs}">{
       for $doc in $docs
            let $lang := data(andoc:FRBRlanguage($doc)/@language)
            group by $lang
            order by $lang
            return <language lang="{$lang}" count="{count($doc)}" />
       }</languages>
       <keywords timestamp="{$ts}" total="{$total-docs}">{
       for $doc in $docs
          for $i in (1 to count(andoc:keywords($doc)))
            let $kw := replace(data(andoc:keywords($doc)[$i]/@value), substring(data(andoc:keywords($doc)[$i]/@value),1,1), upper-case(substring(data(andoc:keywords($doc)[$i]/@value),1,1)))
            group by $kw
            order by $kw
            return <key key="{$kw}" count="{count($doc)}" />
       }</keywords>
       <docType timestamp="{current-dateTime()}" total="{$total-docs}">{
       for $doc in $docs
            let $doctype := data(andoc:document-doctype-generic($doc)/@name)
            group by $doctype
            order by $doctype
            return <type type="{$doctype}" count="{count($doc)}" />
       }</docType>
     </timeline>
};

declare function local:get-query-type($mainQry as document-node()) {
   data($mainQry/query/queryType)
};

declare function local:query-as-xml-doc($qry as xs:string) { 
   let $search-doc := fn:parse-xml(
            util:unescape-uri($qry,"UTF-8")
        )
    return $search-doc 
};

declare function local:get-ft-query($qry as document-node()) {
        if (count($qry/search/phraseQuery) > 0  ) then
            $qry/search/phraseQuery/query
        else
            $qry/search/ftQuery/query
};

declare function local:get-normal-query($mainQry as document-node()) {
   functx:trim(data($mainQry/search/normalQuery))
};

declare function local:advance-search-filter-docs(
    $func as function(item()) as item()*, 
    $count as xs:integer, 
    $from as xs:integer, 
    $received-qry as xs:string
    ) {
    
    let $qry := local:query-as-xml-doc($received-qry)
    (: Now $qry is as Document Node:)
    
    let $normal-query := local:get-normal-query($qry)
    
    return
        if (count($qry/search/normalQuery) > 0 and (count($qry/search/phraseQuery) + count($qry/search/ftQuery))  > 0 ) then
            local:search-ft-and-normal-docs($func,$count,$from,$qry)
       else if ( count($qry/search/phraseQuery) + count($qry/search/ftQuery) > 0) then
            local:search-ft-only-docs($func,$count,$from,$qry)
        else
            local:search-filter-docs($func,$count,$from,$normal-query)
            
}; 
 

declare function local:search-ft-only-docs(
    $func as function(item()) as item()*, 
    $count as xs:integer, 
    $from as xs:integer, 
    $qry as document-node()
    ) {
    
    let $ft-query := local:get-ft-query($qry)
   
   (: let $ft_results := local:lucene-query-ft-or-pharse($all-docs,$ft-query,$splitBy) :)
    
    let $coll := common:doc-fulltext-collection()
    let $docs :=
        for $doc in $coll//gft:pages
            let $connectorID := data($doc/@connectorID)
            let $lucene_pageIDs := $doc/gft:page[ft:query(., $ft-query)]
            let $distinct := fn:distinct-values($lucene_pageIDs)
            return
                if (empty($distinct)) then
                    ()
                else
                    data:doc($connectorID)

      
    let $total-docs := count($docs)
    return
        if ($total-docs gt 0) then
            map {
                "records" := $total-docs,
                "page-size" := $count,
                "items-from" := $from,                    
                "total-pages" := ceiling($total-docs div $count) ,
                "current-page" := xs:integer($from div $count) + 1,
                "data" :=
                    for $s-d in subsequence($docs, $from, $count )
                        return data:summary-doc($s-d)
            }    
        else
            (: $from is greater than the number of available docs :)
            map {
                "records" := 0,
                "total-page" := 0,
                "current-page" := 0,
                "data" := 
                    <bxd:empty>
                        <bxd:message lang="eng">
                        No documents found
                        </bxd:message>
                    </bxd:empty>
            
            }    
};


declare function local:search-ft-and-normal-docs(
    $func as function(item()) as item()*, 
    $count as xs:integer, 
    $from as xs:integer, 
    $qry as document-node()
    ) {
    
    let $all-docs := common:doc-collection()
    
    let $ft-query := local:get-ft-query($qry)
    let $filter-query := local:get-normal-query($qry)
  
    let $qry_docs := util:eval( "$all-docs" || $filter-query || "//an:akomaNtoso/parent::node()" )
    
    (:akn_ft data:) 
    let $coll := common:doc-fulltext-collection()
    let $ftIris :=
        for $doc in $coll//gft:pages
            let $connectorID := data($doc/@connectorID)
            let $lucene_pageIDs := $doc/gft:page[ft:query(., $ft-query)]
            let $distinct := fn:distinct-values($lucene_pageIDs)
            return
                if (empty($distinct)) then
                    ()
                else
                    $connectorID
                    
                     
    (: let $ft_results := local:lucene-query-ft-or-pharse($all-docs,$ft-query,$splitBy) :) 
   
    let $filter-iris := 
        for $doc in $qry_docs
            let $iri := $doc//an:meta/an:identification/an:FRBRExpression/an:FRBRthis/@value/string()
            return $iri
    
    
     
    (:Intersection as we are using &:)
    let $unique-iris := fn:distinct-values($filter-iris[.=$ftIris])
    
    (: Unique IRI's Data :)  
    let $docs := 
        for $current-iri in $unique-iris
                return data:doc($current-iri)
                
    let $total-docs := count($docs)
    return
        if ($total-docs gt 0) then
            map {
                "records" := $total-docs,
                "page-size" := $count,
                "items-from" := $from,                    
                "total-pages" := ceiling($total-docs div $count) ,
                "current-page" := xs:integer($from div $count) + 1,
                "data" :=
                    for $s-d in subsequence($docs, $from, $count )
                        return $func($s-d)
            }    
        else
            (: $from is greater than the number of available docs :)
            map {
                "records" := 0,
                "total-page" := 0,
                "current-page" := 0,
                "data" := 
                    <bxd:empty>
                        <bxd:message lang="eng">
                        No documents found
                        </bxd:message>
                    </bxd:empty>
            
            }    
};


declare function local:search-filter-docs(
    $func as function(item()) as item()*, 
    $count as xs:integer, 
    $from as xs:integer, 
    $qry as xs:string
    ) { 
    let $all-docs := common:doc-collection()
   
    let $docs := util:eval( "$all-docs" || $qry || "//an:akomaNtoso/parent::node()" )
    let $total-docs := count($docs)
    return
        if ($total-docs gt 0) then
            map {
                "records" := $total-docs,
                "page-size" := $count,
                "items-from" := $from,                    
                "total-pages" := ceiling($total-docs div $count) ,
                "current-page" := xs:integer($from div $count) + 1,
                "data" :=
                    for $s-d in subsequence($docs, $from, $count )
                        return $func($s-d)
            }    
        else
            (: $from is greater than the number of available docs :)
            map {
                "records" := 0,
                "total-page" := 0,
                "current-page" := 0,
                "data" := 
                    <bxd:empty>
                        <bxd:message lang="eng">
                        No documents dound
                        </bxd:message>
                    </bxd:empty>
            
            }    
};

declare function local:process-search($func as function(item()) as item()*,  $coll-context, $from as xs:integer, $count as xs:integer, $docs-str as xs:string) {
    let $docs := util:eval($docs-str)
    let $total-docs := count($docs)
    return
        if ($total-docs gt 0) then
           let $docs-in-order := 
                for $doc in $docs
                    order by $doc//an:proprietary/bx:bungenix/bx:dateTime[
                            @refersTo = '#dtModified'
                        ]/@datetime 
                    descending
                return $doc
            return
                map {
                    "records" := $total-docs,
                    "page-size" := $count,
                    "items-from" := $from,                    
                    "total-pages" := ceiling($total-docs div $count) ,
                    "current-page" := xs:integer($from div $count) + 1,
                    "data" :=
                        for $s-d in subsequence($docs-in-order, $from, $count )
                            return $func($s-d)
                }    
        else
            (: $from is greater than the number of available docs :)
            map {
                "records" := 0,
                "total-page" := 0,
                "current-page" := 0,
                "data" := 
                    <bxd:empty>
                        <bxd:message lang="eng">
                        No documents dound
                        </bxd:message>
                    </bxd:empty>
            
            }
};

declare function local:search-language-docs($func, $languages as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll-context := collection($sc("collection"))
    let $languages-count := count($languages)
    let $docs-str := 
        "$coll-context//an:akomaNtoso[.//an:FRBRlanguage[" ||           
             string-join(
                for $language at $p in $languages
                    return
                      if ($p eq $languages-count) then
                        "@language = '" ||  $language  || "' " 
                      else
                         "@language = '" ||  $language  || "' or " 
             )  ||
        "]]/parent::node()"
    return local:process-search(
        $func, 
        $coll-context,
        $from, 
        $count, 
        $docs-str
        )
};

declare function local:search-country-docs($func, $countries as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll-context := collection($sc("collection"))
    let $countries-count := count($countries)
    let $docs-str := 
        "$coll-context//an:akomaNtoso[.//an:FRBRcountry[" ||           
             string-join(
                for $country at $p in $countries
                    return
                      if ($p eq $countries-count) then
                        "@value = '" ||  $country  || "' " 
                      else
                         "@value = '" ||  $country  || "' or " 
             )  ||
        "]]/parent::node()"

    return
        local:process-search(
            $func, 
            $coll-context, 
            $from, 
            $count, 
            $docs-str
        )
            
};


declare function local:search-keyword-docs($func, $kws as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll-context := collection($sc("collection"))
    let $kws-count := count($kws)
    let $docs-str := 
        "$coll-context//an:akomaNtoso[./an:*/an:meta/an:classification/an:keyword[" ||          
             string-join(
                for $kw at $p in $kws
                    return
                      if ($p eq $kws-count) then
                        "@value = '" || $kw  || "'"   
                      else
                        "@value = '" ||  $kw || "' or " 
             )  ||
        "]]/parent::node()"

     return
        local:process-search(
            $func, 
            $coll-context,
            $from, 
            $count, 
            $docs-str
        )
};


declare function local:search-year-docs($func, $years as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll-context := collection($sc("collection"))
    let $years-count := count($years)
    let $docs-str := 
        "$coll-context//an:akomaNtoso[./an:*/an:meta/an:identification/an:FRBRExpression/an:FRBRdate/@date[" ||           
             string-join(
                for $year at $p in $years
                    return
                      if ($p eq $years-count) then
                        "year-from-date(.) = " ||  $year   
                      else
                         "year-from-date(.) = " ||  $year || " or " 
             )  ||
        "]]/parent::node()"
    return
        local:process-search($func, $coll-context, $from, $count, $docs-str)    
};

declare function local:theme-docs($func, $themes as xs:string*, $count as xs:integer, $from as xs:integer) {
    let $sc := config:storage-config("legaldocs")
    let $coll-context := collection($sc("collection"))
    let $themes-count := count($themes)
    let $docs-str := 
        "$coll-context//an:akomaNtoso[.//an:classification[./an:keyword[" ||           
             string-join(
                for $theme at $p in $themes
                    return
                      if ($p eq $themes-count) then
                        "@value = '" ||  $theme  || "' " 
                      else
                         "@value = '" ||  $theme  || "' or " 
             )  ||
        "]]]/parent::node()"
    return
        local:process-search(
            $func, 
            $coll-context, 
            $from, 
            $count, 
            $docs-str
        )    
};

declare function data:save-pkg($iri as xs:string, $doc as item()*, $key as item()*) {
    let $s-map := config:storage-config("legaldocs")
    (: get akn prefixed sub-path :)
    let $db-path := utils:iri-upto-date-part($iri)
    let $log-in := dbauth:login()
    return
        if ($log-in) then
            (: attempt to create the collection, it will return without creating if the collection
            already exists :)
            let $newcol := xmldb:create-collection($s-map("db-path"), $db-path)
            let $fname-xml := utils:get-filename-from-iri($iri, "xml")
            let $fname-key := utils:get-filename-from-iri($iri, "public")
            let $fullpath := concat($s-map("db-path") || $db-path, '/' , $fname-xml)
            let $fullpath-key := concat($s-map("db-path") || $db-path, '/', $fname-key)
            (: store the xml document :)
            return
            try {
                let $stored-doc := util:exclusive-lock(doc($fullpath), (
                    let $stored-doc := xmldb:store($s-map("db-path") || $db-path, $fname-xml, $doc//an:akomaNtoso)
                    return $stored-doc
                ))
                
                (: Store key only if not empty :)
                let $stored-key := 
                if (string-length($key) gt 0) then
                    util:exclusive-lock(doc($fullpath-key), (
                        let $stored-key := xmldb:store($s-map("db-path") || $db-path, $fname-key, $key, 'text/plain')
                        return $stored-key
                    ))
                else
                    true()
                let $logout := dbauth:logout()
                return
                if (empty($stored-doc) or empty($stored-key)) then
                   <return>
                        <error code="save_file_failed" message="error while saving file" />
                   </return>
                else
                   <return>
                        <success code="save_file" message="{$stored-doc}" />
                   </return>
            } catch * {
                <return>
                    <error code="sys_err_{$err:code}" message="Caught error {$err:code}: {$err:description}" />
                </return>
            }
        else
            <return>
                <error code="save_login_failed" message="login to save collection failed" />
            </return>
};

declare function data:delete-pkg($iri as xs:string) {
    let $s-map := config:storage-config("legaldocs")
    (: get akn prefixed sub-path :)
    let $db-path := utils:iri-upto-date-part($iri)
    let $log-in := dbauth:login()
    return
        if ($log-in) then
            let $fname-xml := utils:get-filename-from-iri($iri, "xml")
            let $fullpath := concat($s-map("db-path") || $db-path, '/', $fname-xml)
            let $doc := doc($fullpath)
            let $fname-key := utils:get-filename-from-iri($iri, "public")
            let $fullpath-key := concat($s-map("db-path") || $db-path, '/', $fname-key)

            return
            try {
                (: delete the xml document :)
                let $delete-doc := util:exclusive-lock($doc, (
                    let $delete-doc := xmldb:remove($s-map("db-path") || $db-path, $fname-xml)
                    return $delete-doc
                ))
                
                (: delete key only if present :)
                let $delete-key := 
                if (utils:file-exists($fullpath-key)) then
                    xmldb:remove($s-map("db-path") || $db-path, $fname-key)
                else
                    true()
                let $logout := dbauth:logout()
                return
                if (empty($delete-doc) and (empty($delete-key) or $delete-key)) then
                    <return>
                        <success code="delete_file" message="{$fname-xml}" />
                   </return>
                else
                    <return>
                        <error code="delete_file_failed" message="error while deleting file" />
                   </return>
                   
            } catch * {
                <return>
                    <error code="sys_err_{$err:code}" message="Caught error {$err:code}: {$err:description}" />
                </return>
            }
        else
            <return>
                <error code="delete_login_failed" message="login to delete collection failed" />
            </return>
};

(:
 : Returns a zip of the metadata xml and public key (if present), for the given iri. 
 :)
declare function data:get-pkg($iri as xs:string) {
    let $s-map := config:storage-config("legaldocs")
    (: get akn prefixed sub-path :)
    let $db-path := utils:iri-upto-date-part($iri)
    let $log-in := dbauth:login()
    return
        if ($log-in) then
            let $fname-xml := utils:get-filename-from-iri($iri, "xml")
            let $fullpath := concat($s-map("db-path") || $db-path, '/', $fname-xml)
            let $fname-key := utils:get-filename-from-iri($iri, "public")
            let $fullpath-key := concat($s-map("db-path") || $db-path, '/', $fname-key)

            let $zip as item() := 
            (
                let $entries as item()+ := 
                    if (utils:file-exists($fullpath-key)) then
                        (
                            <entry name="{$fname-key}" type="binary" method="store">{util:binary-doc($fullpath-key)}</entry>,
                            <entry name="{$fname-xml}" type="xml" method="store">{doc($fullpath)}</entry>
                        )                
                    else
                        (<entry name="{$fname-xml}" type="xml" method="store">{doc($fullpath)}</entry>)
                    return
                        compression:zip($entries, false())
            )
            return $zip
        else
            <return>
                <error code="load_login_failed" message="login to load collection failed" />
            </return>
};

(:
 : This Service returns the list of IRIs of published documents 
 : that can be distributed/propagated.
:)
declare function data:dist-list() {
    let $sc := config:storage-config("legaldocs")
    let $coll-context := collection($sc("collection"))
    let $docs := $coll-context//an:akomaNtoso[
        ./an:*/an:meta/an:proprietary/bx:bungenix[
            bx:docDistribute/@origin eq 'bungenix-portal'
            or bx:docDistribute eq 'propagate'
            ]
        ]/parent::node()
    
    return 
        data($docs//an:FRBRExpression/an:FRBRthis/@value)
};


(:
 : Returns other languages in which the given document exists
 :)
declare function data:alt-langs($doc as document-node()) {
    let $coll := common:doc-collection()
    let $cur-uri := andoc:work-FRBRuri-value($doc)
    let $cur-lang := andoc:FRBRlanguage-language($doc)
    let $alt-docs := $coll//an:identification[
                        an:FRBRWork/an:FRBRuri/@value eq $cur-uri
                        and 
                        an:FRBRExpression/an:FRBRlanguage/@language ne $cur-lang
                       ]/parent::node()
            
    let $langs := distinct-values($alt-docs//an:identification/an:FRBRExpression/an:FRBRlanguage/@language)
    
    return
        if (empty($alt-docs)) then
            <bxd:altLangs></bxd:altLangs>
        else
            let $alt-langs := for $lang in $langs
                                return <bxd:altLangs>{$lang}</bxd:altLangs> 
            return $alt-langs
};

