# BungeniX-Data

This the document repository for BungeniX. 

It provides REST services to provide access to the data from other applications, the principal application being the Portal. 

The document repository can reside on the same app-server installation as the Portal, or on a different application server or an entirely different physical server.

## Services

The BungeniX Data servies are primarily provided in XML format.

## Service Namespaces

Document information is returned in a BungeniX XML envelop which has its own namespace:

```
http://bungeni.com/ns/1.0/data
```
This is returned with the `bxd` prefix.

```xml
<bxd:docs xmlns:bxd="http://bungeni.com/ns/1.0/data" timestamp="2017-07-27T11:42:02.796+05:30" orderedby="dt-updated-desc">
    <bxd:summary work-iri="/akn/ke/act/1989-12-15/CAP16/main" expr-iri="/akn/ke/act/1989-12-15/CAP16/eng@2009-07-23/main"/>
    (...)
</bxd:docs>

```

Full documents are returned in the envelop but with the Akoma Ntoso namespace.


